# The flexible DHCP daemon

The goal of this project is to build a flexible DHCP server/relay suitable
for small ISPs running Linux/BSD routers.  It is intended for use in network
infrastructure and is generally not suitable for home usage.

It does not aim to implement a fully-fledged DHCP server (use ISC or Kea
if you need one), but rather provides a flexible way to handle DHCP queries
by referring to upstream servers.  The upstream server can be an external DHCP server,
in which case `flexible-dhcpd` acts like a DHCP relay; or it can be a Radius server, in which case
`flexible-dhcpd` acts like a dumb DHCP server with a Radius backend.

## Motivation

Historically, with dial-up and then xDSL, many ISPs were using L2TP/PPP
coupled with Radius.  It is a well-known stack of protocols, allows
to easily tunnel traffic on various networks, and makes it very easy
to resell service to a downstream ISP.

There are two main free software implementation on the ISP side ("LNS" which
stands for "L2TP Network Server").  [MPD5](http://mpd.sourceforge.net/)
runs on FreeBSD and [l2tpns](http://l2tpns.sourceforge.net/docs/manual/manual.html)
runs on Linux.

However, ISPs and infrastructure providers are moving away from L2TP/PPP
in favour of DHCP/Ethernet/IP.  Reasons include better forwarding performance,
easier support for IPv6, better-designed protocols and software, and increased robustness
(state-keeping is better isolated in DHCP/Radius, while forwarding is mostly
stateless).

In this context, `flexible-dhcpd` aims to be the "swiss army knife" of small ISPs
that need to deal with DHCP/Ethernet/IP on the infrastructure side while using
routers based on Linux/FreeBSD.  Bigger ISPs deal with this new stack
by just buying routers from Cisco (or other providers) that implement all
necessary software.

## Who is behind this project?

The project was started to address the needs of [Rézine](https://www.rezine.org) and
[Illyse](https://www.illyse.net/), two small non-profit ISPs in France.  They are members of the [Fédération FDN](https://www.ffdn.org/),
and more generally are part of a global initiative around [DIY ISPs](https://diyisp.org/dokuwiki/doku.php)
and [community networks](https://communitynetworks.group/).


# Use-cases and scenarios

Below is a set of scenarios for which `flexible-dhcpd` is designed to be useful.

## Scenario 1: stateless edge networks

In this scenario, an ISP deploys a set of interconnected "edge networks"
that provide connectivity to nearby users.  This architecture makes
sense for Wireless ISPs or community networks: each edge network is structured
around a central point (sometimes called a **supernode**) that is typically located
on a high location with good visual coverage.  Users connect to this "supernode"
using directional wireless antennas, multi-hop mesh networks, or even self-deployed
FTTH networks.

Then, all edge networks are interconnected with each other (backbone) and with a core network (backhaul),
typically using long-distance point-to-point wireless links or leased fiber connections.
A routing protocol such as OSPF, Babel or BGP is used to find paths in this network and
route around failures.

![Edge network scenario](diagrams/edge-network-scenario.png)

To assign addresses to end-users, standard protocols are used (DHCP for IPv4,
DHCPv6-PD for IPv6).  Here, community networks often use private IPv4 addresses, but
for [FFDN](https://www.ffdn.org/) members it is important to assign public IPv4 addresses
to every end-user as well as public IPv6 prefixes.  Thus, we need to provision and maintain
a database of which IPv4 address and IPv6 prefix is assigned to who, for instance by identifying
a end-user by the MAC address of its home router.

An important requirement in this use-case is to keep each edge network as simple as possible, with
**no customer-specific configuration in edge routers**.  All customer configuration (authorized MAC address,
public IP address) is kept in a central or redundant database.  This eases maintenance and
deployment, because all edge routers have the same configuration.

Furthermore, adding a new customer only involves writing to the central database,
without any need to know in advance which edge network it will connect to.  We can even
split or merge edge networks without issues.

This requirement can be fulfilled by using a DHCP relay on each edge
router that forwards queries to a central DHCP server with a database of
static leases:

![High-level overview with DHCP](diagrams/edge-network-dhcp.png)

In a modern network, we identify the following requirements for this DHCP relay:

- support for both **IPv4** (DHCP) and **IPv6 prefix delegation** (DHCPv6-PD)
- be able to **assign /32 IPv4 addresses** to clients (using [DHCP option 121](https://tools.ietf.org/html/rfc3442))
- when it assigns an IPv4 address or an IPv6 prefix, it needs to **add
  a route to the kernel** which is then propagated by the routing protocol.
  This makes sure that the address of the customer is reachable from the
  whole network (and consequently from anywhere in the Internet)
- conversely, this route should be removed when the lease expires
- be able to **query one or more upstream DHCP servers** when receiving a
  DHCP request, with various strategies (round-robin, fallback)
- after the initial upstream query for a client, keep a **cache** of the
  answer (e.g. IP address associated to the client MAC address), so that
  we don't need to query the upstream server again for each DHCP renew.
  This allows continued operation even if the DHCP server is down or
  unreachable.  However, this cache should be refreshed from time to time
  to stay up-to-date with the upstream server: for instance, a customer
  may want to change its IP address in the central database and this
  should be reflected in the network after a given amount of time.

Using a cache in a DHCP relay is usually a bad idea, because the central
DHCP server needs to be aware that an IP address is still in use to avoid
dynamically assigning it to somebody else.
However, in our use-case, we assume that all IP addresses are assigned statically
in the central database ("static leases"), so the DHCP server will not decide
to dynamically assign apparently-unused IP addresses to somebody else.

To our knowledge, no existing free software is flexible enough to fit all
these requirements.

## Scenario 1bis: Radius variant

In the previous scenario, the central database was only used for two purposes:

- is the given customer allowed to connect to the network? (based on its MAC address or other information)
- if yes, which IPv4 address and IPv6 prefix should be assigned to this customer?

Using a DHCP server with static leases can certainly work, but this a job that
Radius can fulfill perfectly!  The figure below is almost similar to the
first scenario, but we replaced the central DHCP server by a Radius server:

![High-level overview with Radius](diagrams/edge-network-radius.png)

The software running on the edge router has almost the same role: whenever
it receives a DHCP request from a customer, it queries the upstream Radius
server for guidance.

**All previously identified requirements still hold**, and we have a new one:

- **flexible authentication mechanism**: when receiving a DHCP request
  from a client, we can choose which parameters we include in our Radius
  query to identify the customer.  It can simply be the MAC address of the
  client, or we can copy a DHCP option that has been inserted by a layer-2
  device.  The "Circuit-ID" option is commonly inserted by OLTs, and this
  can be translated to a corresponding Radius option such as
  "ADSL-Agent-Circuit-id".  If you want to know more about the concrete
  usage of the Circuit-ID DHCP option and its Radius counterpart, see the
  [bitstream documentation from a FTTH infrastructure provider, Axione, in French](http://www.lafibrepaloise.fr/files/Editeur/La%20Fibre%20Paloise/documents/CP%20FTTH%20Active%20Annexe%202_STAS%20v18.01.pdf).

Rézine has been builing a wireless network following such an "edge-core"
design.  We initially used Mikrotik routers because their DHCP server can
query a Radius server, but it turned out to be too limiting (there is no
support for DHCPv6 + Radius).  The same network design is now running with
[Kea](https://kea.isc.org/) on Debian, with
[custom code to query a Radius server](https://code.ffdn.org/zorun/kea/commits/radius_v0.2)
and
[a hook to insert routes into the kernel](https://github.com/zorun/kea-hook-runscript/).
However, this solution was hard to implement and turns out to be very
difficult to maintain, and Rézine is looking for a simpler and more
flexible solution.

## Scenario 2: dispatching requests to the right ISP

Illyse is an ISP using the "bitstream" service of a FTTH infrastructure
provider.  This service follows a dual-stack DHCP/Ethernet/IP + Radius
model (see the
[technical documentation for the bitstream service from Axione](http://www.lafibrepaloise.fr/files/Editeur/La%20Fibre%20Paloise/documents/CP%20FTTH%20Active%20Annexe%202_STAS%20v18.01.pdf),
in French).

Problem: this bitstream service needs to be shared among several
non-profit ISPs.  Each ISP contracts with users in its own geographical
area, then asks the infrastructure provider to setup the connection at the
customer's home.  Then, the infrastructure provider forwards all traffic
from all customers to a single interconnection point in a datacenter.  At
this point, how to dispatch DHCP/Radius requests and traffic to the right
ISP?

With L2TP/PPP, it was relatively easy to do with a Radius proxy and terminating
L2TP sessions for each customer to the right ISP.  We even
[documented how to set it up](https://www.grenode.net/Documentation_technique/R%C3%A9seau/Collecte_xDSL/).

With this new protocol stack, we can still use a Radius proxy to dispatch
Radius queries to the right ISP.  However, we also need to do something
similar for DHCP, with the following requirements:

- **receive DHCP requests** from the third-party infrastructure provider
- **dispatch DHCP requests** to the right ISP based on some options of the
  incoming requests (DHCP option such as Circuit-ID, MAC address...)
- **relay back DHCP responses** to the infrastructure provider
- possibly **insert routes in the kernel** when relaying the responses

The last step is to forward customer traffic to the right ISP.  This can
be achieved with policy routing and is out of scope of `flexible-dhcpcd`.


# General design goals

In addition, here are some general design goals for `flexible-dhcpd` that apply to all scenarios:

- **IPv4/IPv6 feature parity**
- Target **Linux** systems, but try to be portable to **BSD**
- **Reasonable memory/storage footprint**: the software should run on both
  classical server OS (Debian) but also on reasonably capable embedded systems
  running OpenWrt.  This may be useful for small edge networks such as Wireless ISPs.
- **Integrate with routing protocols**: at a minimum, the software needs
  to be able to insert/remove routes in the kernel based on DHCP
  responses.  These routes can then be picked up by a routing daemon such
  as [Bird](https://bird.network.cz/) and propagated to the rest of the
  network thanks to a routing protocol (OSPF, BGP, Babel...)  An
  alternative would be to implement BGP directly in `flexible-dhcpd` and
  announce the routes over a BGP session.  An advantage is that the routes
  would be automatically withdrawn on shutdown/crash/restart of the daemon.
- Support for **several upstream models** for Radius and DHCP:
  round-robin, dead server detection, query duplication, upstream
  selection based on DHCP option...

In short: **maximum flexibility!**


# Implementation

Given the requirements, languages such as Python or Go should be avoided (too high footprint).

The core will probably end up being written in C, or maybe Rust.

Then, the configuration language needs some thinking to achieve good flexibility.
We need at least the following configuration possibilities:

- which **upstream(s)** to use: DHCP or Radius, IP addresses, query strategy (redundant queries,
  round-robin, fallback, dead server detection...)
- given a DHCP request from a client (with a number of elements/options),
**what query should we send** to an upstream server, and **which upstream
server** should we contact?  This is the part that requires the most
flexibility.
- behaviour of the **cache**: time-to-live of positive responses, negative
  responses...
- how **routes** are inserted/advertised: in which routing table, with
  which kernel proto, should they remain in the kernel when the daemon
  exits, etc

Either an elaborate configuration language needs to be specified and
implemented, which is a lot of work, or we could reuse an existing
flexible language (Lua ?).
